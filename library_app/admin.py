from django.contrib import admin

from library_app.models import Book


@admin.register(Book)
class AdminBooks(admin.ModelAdmin):
    class Meta:
        model = Book
