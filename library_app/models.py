from django.db import models

from users.models import User


class Book(models.Model):
    name = models.CharField(max_length=45,)
    author = models.CharField(max_length=25)
    reader = models.ForeignKey(User, on_delete=None, null=True,
                               blank=True)
