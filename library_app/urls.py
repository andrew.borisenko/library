from django.urls import path
from library_app import views

urlpatterns = [
    path('books_list/', views.GetBooksList.as_view()),
    path('take_book/', views.TakeBook.as_view()),
    path('return_book/', views.ReturnBook.as_view()),
    path('my_books/', views.MyBooksList.as_view()),
]