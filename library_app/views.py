from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from library_app.models import Book
from library_app.serializers import BookSerializer, TakeBookSerializer, \
    ReturnBookSerializer


class MyBooksList(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = BookSerializer

    def get(self, request):
        query = Book.objects.filter(reader=request.user)
        serializer = self.serializer_class(query, many=True)
        return Response(serializer.data)


class GetBooksList(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = BookSerializer

    def get(self, request):
        query = Book.objects.all()
        serializer = self.serializer_class(query, many=True)
        return Response(serializer.data)


class TakeBook(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = TakeBookSerializer

    def put(self, request):
        serializer = TakeBookSerializer(data=request.data)
        serializer.context.update({"user": request.user})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.take_book())


class ReturnBook(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ReturnBookSerializer

    def put(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.context.update({"user": request.user})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.return_book())