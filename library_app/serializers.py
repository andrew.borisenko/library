from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from library_app.models import Book


class BookSerializer(serializers.ModelSerializer):
    reader = serializers.SerializerMethodField()
    ability = serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = "__all__"

    def get_ability(self, book_instance):
        return False if book_instance.reader else True

    def get_reader(self, book_instance):
        if book_instance.reader:
            return book_instance.reader.first_name + " " + \
                   book_instance.reader.last_name


class TakeBookSerializer(serializers.Serializer):
    book_id = serializers.IntegerField(required=True)
    def validate(self, data):

        if Book.objects.get(id=self.initial_data.get(
                "book_id")).reader is not None:
            raise ValidationError("Another user is reading this book now.")

        data.update(
            {
                'book_id': self.initial_data.get('book_id'),
                'user': self.context.get('user')
            }
        )
        return data

    def take_book(self):
        Book.objects.filter(id=self.data.get('book_id')).update(
            reader=self.validated_data.get('user'))
        return BookSerializer(instance=Book.objects.get(
            id=self.data.get('book_id'))).data


class ReturnBookSerializer(serializers.Serializer):

    book_id = serializers.IntegerField()

    def validate(self, data):
        book_instance = Book.objects.get(id=self.initial_data.get("book_id"))
        if book_instance.reader is None:
            raise ValidationError("Incorrect book_id. "
                                  "Book is already in the library")
        elif book_instance.reader != self.context.get("user"):
            raise ValidationError("Incorrect book_id. "
                                  "You can return only your books")
        else:
            return {
                "book_id": self.initial_data.get('book_id'),
                "owner": self.context.get("user")
            }

    def return_book(self):
        Book.objects.filter(id=self.data.get('book_id')).update(
            reader=None)
        return BookSerializer(instance=Book.objects.get(
            id=self.data.get('book_id'))).data