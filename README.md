# library
Project for easy library management
To launch project follow the steps below

1. download project with command  "git clone"
2. go to downloading directory on your PC "cd library"
3. build docker image with command "docker build -t library . "
4. run docker-compose file with command "docker-compose up"
5. run command for applying migration inside your docker container with command "docker container exec "id_of_your_container" python manage.py migrate"

After success project running you have ability to:

create user; 
log in with created user; 
see list of all books; 
take some books; 
see list of your books; 
return your book to library.