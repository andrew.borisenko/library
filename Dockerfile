FROM python:3.6-alpine

RUN apk add postgresql-dev gcc musl-dev

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip3 install -r requirements.txt
